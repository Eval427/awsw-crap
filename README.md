# AwSW - Community Resource and Assets Pack

Community Resource and Assets Pack (CRAP) is a community project for the game Angels with Scaly Wings focused on creating a mod, which any modder can use as a dependency of their mod, giving them access to extra sprites, backgrounds, sounds, etc. created by other members of the modding community.

