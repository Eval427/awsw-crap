from modloader import modast, modinfo
from modloader.modclass import Mod, loadable_mod

@loadable_mod
class AWSWMod(Mod):
    name = "Community Resource and Assets Pack"
    version = "0.1.0"
    author = "AwSW Community"
    
    def mod_load(self):
        pass
    
    def mod_complete(self):
        pass
